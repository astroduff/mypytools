#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Routine for converting Gadget3 HDF5 SubFind output to TIAMAT's binary format."""

import pandas as pd
import numpy as np
import numexpr as ne
import h5py
from calculateradius import *
from calculatevcirc import *
from calculatediff import *
from pyread_gadget_hdf5 import *
from pyread_header_hdf5 import *

import fnmatch
import struct
import os
import sys
from collections import OrderedDict

from numpy import uint8, uint32, uint64, float32

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.2.0'

# kernprof -l profiling.py 
# python -m line_profiler profiling.py.lprof 
#@profile

def subfindtotiamat(filename, longlong=None, noloop=None, silent=None, debug=None, reslimit=None, subdivide=None):
    """ Convert the SubFind Gadget3 HDF5 output to binary format for TIAMAT's binary format."""

    """
    +
     NAME:
            SUBFINDTOTIAMAT
    
     PURPOSE:
            This function converts HDF5 SubFind outputs of the OWLS Gadget3 code to Binary TIAMAT format

     CATEGORY:
            I/O, HDF5, TIAMAT / PAISTE

     REQUIREMENTS:
            import numpy as np
            import h5py
            from pyread_gadget_hdf5 import *
            from pyread_header_hdf5 import *
            import struct 
            import os
            import sys

    
     CALLING SEQUENCE:
            from hdf5totiamat import *
            Result = subfindtotiamat(filename [, longlong=True, noloop=True, silent=True, reslimit=True] )
    
     INPUTS:
           filename: Name of the file to read in. In case of a multiple files
                     output, any sub-file name can be given. The routine will
                     take care of reading data from each sub-file. Unless [noloop=True]

     OPTIONAL INPUTS:
    
     KEYWORD PARAMETERS (default set to None)
           silent:      does not print any message on the screen
           debug:       if true will perform additional debug checks and prints
           longlong:    force int to be LongLong (code checks if needed however)
           noloop:      limit the read in to only the file selected (at this stage this is never triggered
                        as subfind has an issue with subdivided files that mean it doesn't share particles
                        and group properties correctly)
           reslimit:    Min number of DM particles in a group to be retained. 
                        If left as None, then sets to zero, if user selects True, defaults to 20, else takes user number passed. 
           subdivde:    If set to True this will perform subdivision of the output files, default is None to use the Tiamat old format.
    
     OUTPUTS:
            A series of binary file in the '/data/' directory containing the SubFind output. Only using DM particles.
            If file is gas+DM then everything is recalulated (including resetting ParticleIDs) for the DM only.
    
     RESTRICTIONS:
    
    
     PROCEDURE:
    
    
     EXAMPLE:
        
            Convert the SubFind output to TIAMAT format:
            from hdf5totiamat import *
            subfindtotiamat('/Users/aduffy/OWLS/REF_L025N128/data/subhalos_014/subhalo_014.0.hdf5')    
        
    
     MODIFICATION HISTORY (by Alan Duffy):
            
            1/10/13 Created the SubFind to TIAMAT conversion script
            11/03/15 Added subdivide keyword. Set to False to ensure compatability with gbPoole software (i.e. TIAMAT)
            12/03/15 Significant bug fixes in indexing and updates to pandas 0.15 (which broke a lot of indexing)
            4/04/15 x5 speed up through code modification and runfast keyword added to skip Spin calculation
            5/04/15 x10 speed up through code modification to calculate spin and remove runfast keyword 

            Any issues please contact Alan Duffy on mail@alanrduffy.com or (preferred) twitter @astroduff
    """



#########################################################
##
## Define the Group Properties struct
##
#########################################################
    BINARY_TIAMAT_DATAFORMAT= OrderedDict(\
        [('id_MBP', uint64), ('M_vir', float64), ('n_particles', uint32), ('position_COM', (float32, 3)), 
        ('position_MBP', (float32, 3)), ('velocity_COM',(float32, 3)), ('velocity_MBP',(float32, 3)), 
        ('R_vir', float32), ('R_halo', float32), ('R_max', float32), ('V_max', float32), ('sigma_v', float32),
        ('spin', (float32, 3)), ('q_triaxial', float32), ('s_triaxial', float32), 
        ('shape_eigen_vectors', (float32, 9)), ('padding', uint8) ])
    BINARY_TIAMAT_DATATYPE = np.dtype({'names':BINARY_TIAMAT_DATAFORMAT.keys(), \
                                    'formats':BINARY_TIAMAT_DATAFORMAT.values()}, \
                                    align = True)

#########################################################
##
## Check user choices...
##
#########################################################
    mingr = 2
    if not reslimit:
        reslimit = 1 # As I test for >= particles, and want to exclude zero.
    elif reslimit == True:
        reslimit = 20
    else:
        reslimit = reslimit

    if not silent:
        if reslimit == 1:
            print "Select all objects in catalogue (with at least 1 particle, i.e. all)"
        else:
            print "Minimum number of particles for object to be considered is ", reslimit

    if not silent:
        print "User gave "+filename

    ## Determine the file type, i.e. SubFind, Snapshot or old FOF.
    if filename.rfind('/subhalo') >= 0:
        inputtype = 'SubFind' ## Define File type
        inputname = 'subhalo'
        inputfolder = 'subhalos'
    elif filename.rfind('/group') >= 0:
        inputtype = 'FoF' ## Define File type
        inputname = 'group' 
        inputfolder = 'groups'
    elif filename.rfind('/snap') >= 0:
        inputtype = 'Snapshot' ## Define File type
        inputname = 'snap'
        inputfolder = 'snapshot'
    if not silent:
        print "This is a "+inputtype+" output filetype"

    if inputtype != 'SubFind':
        print "This only works for SubFind output "
        return -1


#########################################################
##
## Determine if, and how many, subfiles we must loop over
##
#########################################################

    folder_index = filename.rfind(inputname)
    snapnum_index = filename[:folder_index-1].rfind('_')
    snapnum = int(filename[snapnum_index+1:folder_index-1])
    numfile = len(fnmatch.filter(os.listdir(filename[:folder_index]), '*.hdf5'))
    if noloop:
        numfileloop = 1 ## Force the code to only consider the input file
    else:
        numfileloop = numfile

    print "We will consider "+str(numfileloop)+" subfiles"

    ## !! Can't consider SubFind split files alone, 
    ## !! particles in groups aren't stored on each as they're shared over subfiles.
    print "SubFind split files don't match particles present to the groups, can't subdivide "
    numfileloop = 1
    noloop = None

    folder_index = filename.rfind(inputfolder)
    for subfile in range(0,numfileloop):
        folder = filename[0:folder_index]
        if not silent:
            print "Folder is "+folder

        if filename.rfind('/data') >= 0:
            outfolder_index = filename.rfind('/data')
            outfolder = filename[0:outfolder_index] + '/'
        else:
            outfolder = folder
        if not silent:
            print "Output folder is "+outfolder

        infile = 'subhalos_'+str(snapnum).zfill(3)+'/subhalo_'+str(snapnum).zfill(3)+'.'+str(subfile)+'.hdf5'

        Header = pyread_header_hdf5(folder+infile, silent=silent)    

        ## Calculate Cosmology
        BoxSize = Header['BoxSize']
        HalfBoxSize = BoxSize/2.
        NegHalfBoxSize = HalfBoxSize * -1.
        DMpartMass = Header['MassTable'][1]
        Redshift = np.array( Header['Redshift'] )
        Omega0 = np.array( Header['Omega0'] )
        OmegaLambda = np.array( Header['OmegaLambda'] )
        OmegaBaryon = np.array( Header['OmegaBaryon'] )
        H_of_o = np.array( Header['HubbleParam']*100. ) ## Current Day Hubble Parameter 
        H_of_z = np.sqrt( H_of_o**2. * ( (Omega0 * (1.+Redshift)**3.)\
        + ( (1. - (Omega0+OmegaLambda)) * (1.+Redshift)**2.) + OmegaLambda)) ## Hubble Parameter at z
        scalefactor = np.array( 1. / (1. + Redshift) )
        sqrt_scalefactor = np.sqrt( scalefactor )

        if not silent:
            print "At redshift ", Redshift, "or scalefactor ",scalefactor, " the Hubble Flow is ", H_of_z

        if OmegaBaryon < 0.001:
            if not silent:
                print "No Baryons Present, DMONLY=True "
            dmonly = True
        else:
            if not silent:
                print "Assuming Baryons Present, DMONLY=None "
            dmonly = None

        ## Calculate Units
        with h5py.File(folder+infile, "r") as fin:
            Utime = fin['Constants'].attrs['SEC_PER_YEAR']
            Umass = fin['Constants'].attrs['SOLAR_MASS']
            Ulength = fin['Constants'].attrs['CM_PER_MPC']
            CM_PER_KM = 1e5
            NewtonG = fin['Constants'].attrs['GRAVITY'] ## cgs [cm^3 g^-1 s^-2]
            NewtonG_kmssq_MpcMsol = NewtonG / (CM_PER_KM**2. * Ulength * Umass**-1.)
            NewtonG /= (Ulength**3. * Umass**-1. * Utime**-2.) ## Astro [Mpc^3 Msol^-1 yr^-2]
            if not silent:
                print "Newton's Constant in [Mpc^3 Msol^-1 yr^-2] is ",NewtonG
                print "Newton's Constant in [(km/s)^2 Mpc Msol^-1] is ",NewtonG_kmssq_MpcMsol

#########################################################
##
## If the Simulation has baryons, need to remove the Baryon IDs
## can also check for total particle number, i.e. if Long Long INT
## is required
##
#########################################################
        if dmonly:
            BaryonFraction = 0.
            ConvFactor = np.array(1.)
        else:
            BaryonFraction = OmegaBaryon/Omega0
            ConvFactor = np.array(1./(1.-BaryonFraction))
        ## Need to calculate the number of baryon particles in the sim, and remove these.
        part_folder_index = filename.rfind('/data')
        part_index = filename[:part_folder_index-1].rfind('N')
        npart_name = (long(filename[part_index+1:part_folder_index]))**3
        ## Make a sanity check for the number calculated.
        MassInBox = BoxSize**3. * (3. * 100.**2)/(8.*np.pi*NewtonG_kmssq_MpcMsol)
        npart = MassInBox * (Omega0-OmegaBaryon)/ ( 1e10 * DMpartMass )
        if abs(npart/npart_name -1.) > 0.001:
            print "Something is wrong with the calculated particle numbers "
            print "The name suggests ", npart_name**(1./3.)
            print "But I calculate ", npart**(1./3.)
        if dmonly:
            npart_total = npart_name
        else:
            npart_total = 2.*npart_name        
            if not silent:
                print "Must increase Particle Mass by ",ConvFactor

#########################################################
##
## Determine number of particles require Long Long INT
##
#########################################################
        if sum(Header['NumPart_Total'].astype(uint64)) < 1. and subfile == 0:
            if not silent:
                print "Force counting due to SubFind bug"
            ## Bug in SubFind output, summate by hand
            for subloop in range(0,numfile):
                infile = 'subhalos_'+str(snapnum).zfill(3)+'/subhalo_'+str(snapnum).zfill(3)+'.'+str(subloop)+'.hdf5'
                Global_Header = pyread_header_hdf5(folder+infile, silent=silent)
                if subloop == 0:
                    Global_NumPart_Total_HighWord = Global_Header['NumPart_Total_HighWord']
                    Global_NumPart_Total = Global_Header['NumPart_ThisFile']
                else:
                    Global_NumPart_Total += Global_Header['NumPart_ThisFile']
                    Global_NumPart_Total_HighWord += Global_Header['NumPart_Total_HighWord']
            Header['NumPart_Total'] = Global_NumPart_Total
            Header['NumPart_Total_HighWord'] = Global_NumPart_Total_HighWord

        if sum(Header['NumPart_Total'].astype(uint64))+sum(Header['NumPart_Total_HighWord'].astype(uint64)) > 2.*1024.**3. or npart_total > 2.*1024.**3.:
            if not silent:
                print "Long long int needed "
            intsz = uint64 ## Unsigned long long
            intstr = 'Q' ## Unsigned long long
            bytesz = 8
        else:
            if not silent:
                print "Long int only "
            intsz = uint32
            intstr = 'I' ## Unsigned int
            bytesz = 4

        if longlong:
            if not silent:
                print "Long long int needed "
            intsz = uint64
            intstr = 'Q' ## Unsigned long long
            bytesz = 8

        if not silent:
            if noloop:
                print "Number of particles in file",Header['NumPart_ThisFile'][1]
            else:
                print "Number of particles in file",Header['NumPart_Total'][1]
 
#########################################################
##
## Create the SubFind Group Catalogue File
##
#########################################################
        if not silent:
            print "Now SubFind Group Info "

        if subdivide:
            ## Create a directory (if needed) and copy subfivided files out
            out_directory = outfolder+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups.'+str(subfile)
        else:
            if subfile > 1:
                print "Warning: Can't append multiple subfiles to the same Tiamat single file yet"
            out_directory = outfolder+'halos/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            ## Create a single file, with the name of the subdived head directory, to hold all info 
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups'

        ## Read the SubFind Group Info 
        Header = pyread_header_hdf5(folder+infile, silent=silent, subheader=True)
        
        TotNumFoFGroups = 0
        TotNumSubGroups = 0
        NumFoFGroups = 0
        NumSubGroups = 0

        OrigNumSubGroups = Header['Total_Number_of_subgroups'].astype(intsz)
        if OrigNumSubGroups > mingr:
            ## Have to read in the entire array to know how many to expect, annoying
            SUBlength = pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', sub_dir='FoF', nopanda=True, silent=silent).flatten()
            SUB_gtreslim = SUBlength >= reslimit
            SUB_gtreslim = np.where(SUB_gtreslim == True)
            SUB_gtreslim = np.asarray(SUB_gtreslim).flatten()
            SUBlength = SUBlength[SUB_gtreslim]
            TotNumSubGroups = len(SUBlength)
            if TotNumSubGroups > 0:
                GrNrGroup = pyread_gadget_hdf5(folder+infile, 10, 'GrNr', sub_dir='SubFind', nopanda=True, silent=silent).flatten()
                FOF_gtreslim = np.unique(GrNrGroup[SUB_gtreslim])
                TotNumFoFGroups = len(FOF_gtreslim) ## Can have multiple subhaloes in one FOF
        else:
            print "There are only ",OrigNumSubGroups, " subggroups, going to have to output empty files as need ",mingr
            TotNumSubGroups = 0
            TotNumFoFGroups = 0
#            break ## Nothing to read so cancel

        if noloop:
            OrigNumSubGroups = Header['Number_of_subgroups'].astype(intsz)
            if OrigNumSubGroups > mingr:
                ## Only open if enough!
                SUBlength = pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent).flatten()
                SUB_gtreslim = SUBlength >= reslimit
                SUBlength = SUBlength[SUB_gtreslim]
                NumSubGroups = len(SUBlength)

                if NumSubGroups > 0:
                    GrNrGroup = pyread_gadget_hdf5(folder+infile, 10, 'GrNr', noloop=noloop, sub_dir='SubFind', nopanda=True, silent=silent).flatten()
                    SUB_gtreslim = np.where(SUB_gtreslim == True)
                    SUB_gtreslim = np.asarray(SUB_gtreslim).flatten()
                    FOF_gtreslim = np.unique(GrNrGroup[SUB_gtreslim])
                    NumFoFGroups = len(FOF_gtreslim) ## Can have multiple subhaloes in one FOF
            else:
                print "There are only ",OrigNumSubGroups, " subggroups, going to have to output empty files as need ",mingr
                NumSubGroups = 0
                NumFoFGroups = 0
                #break ## Nothing to read so cancel

        else:
            # All subhaloes available will be outputted to the final file, so individual count is total count
            NumSubGroups = TotNumSubGroups
            NumFoFGroups = TotNumFoFGroups

        if not silent:
            if noloop:
                print "There are ",NumSubGroups, " subhalos and ",NumFoFGroups, " FOF groups in this file "
            else:
                print "There are ",NumSubGroups, " subhalos and ",NumFoFGroups, " FOF groups in this entire subfind output "

        if NumSubGroups == 0 and NumFoFGroups > 0:
            closeparticles = None
        else:
            closeparticles = True

        if NumSubGroups > 0 and NumFoFGroups > 0:
            OrigFOFlength = pyread_gadget_hdf5(folder+infile, 1, 'Length', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent).flatten()
            FOFlength = OrigFOFlength[FOF_gtreslim] ## So this is the length of the FOF groups that have at least a subhalo

            if len(FOFlength) != NumFoFGroups:
                print "There are ",NumFoFGroups, " FOF groups and yet ",len(FOFlength), " length"
            
            ## Create a simple incremental increase for the new FOF particle list 
            FOFoffset = FOFlength.cumsum()
            FOFoffset = np.roll(FOFoffset, 1)
            FOFoffset[0] = 0

            ## So this is the offset in the original particle list of FOF groups that have at least a subhalo
            OrigFOFoffset = pyread_gadget_hdf5(folder+infile, 1, 'Offset', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent).flatten()

            ## Can access a FOF particle list in the original format as
            ## for GrNr in FOF_gtreslim:
            ##  particle = particlelist[OrigFOFoffset[GrNr]:OrigFOFoffset[GrNr]+OrigFOFlength[GrNr]-1]
            if len(OrigFOFoffset[FOF_gtreslim]) != NumFoFGroups:
                print "Selected ", len(OrigFOFoffset[FOF_gtreslim]), " groups in FOF offset array when it should be ", NumFoFGroups
            
            ## So this is the offset in the original particle list of subhaloes that have at least reslimit particles
            OrigSUBoffset = pyread_gadget_hdf5(folder+infile, 1, 'SUB_Offset', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent).flatten()
            OrigSUBlength = pyread_gadget_hdf5(folder+infile, 1, 'SUB_Length', noloop=noloop, sub_dir='FoF', nopanda=True, silent=silent).flatten()
            ## for GrNr in SUB_gtreslim:
            ##  particle = particlelist[OrigSUBoffset[GrNr]:OrigSUBoffset[GrNr]+OrigSUBlength[GrNr]-1]
            if len(OrigSUBoffset[SUB_gtreslim]) != NumSubGroups:
                print "Selected ", len(OrigSUBoffset[SUB_gtreslim]), " groups in Subhalo offset array when it should be ", NumSubGroups

            ## To create the new subhalo offset list have to start count at FOF halo offset
            if longlong:
                SUBoffset = np.zeros(NumSubGroups, dtype=np.long)
            else:
                SUBoffset = np.zeros(NumSubGroups, dtype=np.int)

            NsubPerHalo = np.zeros(NumFoFGroups, dtype=np.int)
            FOFGrNr = 0
            for SUBindex, SUBnumber in enumerate(SUB_gtreslim):
                if SUBindex > 0:
                    if GrNrGroup[SUBnumber] != GrNrGroup[SUBnumber-1]: 
                        ## First subhalo of the FOF group
                        FOFGrNr += 1
                        SUBoffset[SUBindex] = FOFoffset[FOFGrNr]
                    else:
                        ## The subhalo is not the main first subhalo in the array so add the last length
                        SUBoffset[SUBindex] += SUBoffset[SUBindex-1] + SUBlength[SUBindex-1]
                ## Keep track of the number of subhaloes in the output FOF array format
                NsubPerHalo[FOFGrNr] += 1
#                print "SUBindex=",SUBindex," SUBnumber=",SUBnumber," for GrNrGroup ",GrNrGroup[SUBnumber],\
#                    " SUBoffset=",SUBoffset[SUBindex]," SUBlength=",SUBlength[SUBindex],\
#                    " FOFoffset=",FOFoffset[FOFGrNr], " nsubhalo=",NsubPerHalo[FOFGrNr]
            if (FOFGrNr+1) != NumFoFGroups:
                print "Didn't iterate over all the groups "
                print "Should have tracked ",NumFoFGroups," instead did ",FOFGrNr

            if np.sum(NsubPerHalo) != NumSubGroups:
                print "The total number of subhaloes ", np.sum(NsubPerHalo), " is not equal to number of subhaloes ", NumSubGroups

            if longlong:
                SUBoffset = SUBoffset.astype(long) ## Recast as int, likely because missing data in Length forces change to float
                FOFoffset = FOFoffset.astype(long) ## Recast as int, likely because missing data in Length forces change to float
            else:
                SUBoffset = SUBoffset.astype(int) ## Recast as int, likely because missing data in Length forces change to float
                FOFoffset = FOFoffset.astype(int) ## Recast as int, likely because missing data in Length forces change to float
    
            nfluff = np.sum(FOFlength) - np.sum(SUBlength)

        if NumSubGroups == 0:
            print "There are no resolved Subgroups, output empty file"
            #break

        ## Write the SubFind Catalog
        print "Write the Subfind catalog_subgroups"
        if not silent:
            print "Writing to "+particles_outfile
        if NumSubGroups > 0:
            if NumSubGroups != len(SUBlength):
                print "Number subgroups not correct "
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving N_groups, the number of SubFind groups in the file
            fout.write(struct.pack('i', NumSubGroups))
            ## write 4-byte integer giving the number of bytes used for the group particle offsets (N_b_go)
            fout.write(struct.pack('i', bytesz))
            ## Write Body, 2-arrays, each N_b_go long

            ## write the Length array
            if NumSubGroups > 0:
                fout.write(SUBlength.astype(uint32))
            else:
                fout.write(np.array([0]).astype(uint32))
            ## write the Offset array
            if NumSubGroups > 0:
                fout.write(SUBoffset.astype(intsz)) 
            else:
                fout.write(np.array([0]).astype(uint32))

        if not silent:
            print "Wrote SubFind Group file"

#########################################################
##
## Create the SubFind Subhalo Properties 
##
#########################################################

        if not silent:
            print "Now SubFind Properties Info "

        if subdivide:
            ## Create a directory (if needed) and copy subfivided files out
            out_directory = outfolder+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups_properties/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups_properties.'+str(subfile)
        else:
            if subfile > 1:
                print "Warning: Can't append multiple subfiles to the same Tiamat single file yet"
            ## Create a single file, with the name of the subdived head directory, to hold all info 
            out_directory = outfolder+'catalogs/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_subgroups_properties'

        print "Write the Subfind catalog_subgroups_properties"
        ## Write the SubFind Catalog
        if not silent:
            print "Writing to "+particles_outfile
        if NumSubGroups > 0:
            if NumSubGroups != len(SUBlength):
                print "The number of subhaloes is ", NumSubGroups, " while the length of SUBlength is ",len(SUBlength)
            if NumSubGroups != len(SUBoffset):
                print "The number of subhaloes is ", NumSubGroups, " while the length of SUBoffset is ",len(SUBoffset)
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving i_sub, the number of this file in the list of n_sub subfiles
            fout.write(struct.pack('i', subfile)) 
            ## write 4-byte integer giving n_sub, the number of subfiles in the directory
            fout.write(struct.pack('i', numfile)) 
            ## write 4-byte integer giving i_groups, the number of subhaloes in this file
            fout.write(struct.pack('i', NumSubGroups))
            ## write 4-byte integer giving n_groups, the number of subhaloes in all files
            fout.write(struct.pack('i', TotNumSubGroups))

#########################################################
##
## Have to calculate the SubFind Subhalo Properties 
##
#########################################################
            if NumSubGroups > 0:
                ## Now read in the ParticleIDs
                particles=pyread_gadget_hdf5(folder+infile, 1, 'ParticleIDs', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'ParticleIDs'})
                if dmonly:
                    if not silent:
                        print "There's only DM so don't change ParticleIDs"
                else:
                    if not silent:
                        print "Reset the DM ParticleIDs, removing the gas count "
                    particles.ParticleIDs -= npart_name

                ## Coordinates are COMOVING [Mpc/h]
                particles=particles.join( \
                    pyread_gadget_hdf5(folder+infile, 1, 'Coordinates', noloop=noloop, sub_dir='FoF', silent=silent).\
                    rename(columns={0:'PosX', 1:'PosY', 2:'PosZ'}) )

                ## Velocites are PROPER [km/s]
                particles=particles.join( \
                    pyread_gadget_hdf5(\
                        folder+infile, 1, 'Velocity', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True).\
                    rename(columns={0:'VelX', 1:'VelY', 2:'VelZ'}) )

                ## Masses are 'proper' (of course) [Msol/h]  and correct for missing baryon particles (if neccessary)
                particles['Mass']=pyread_gadget_hdf5(\
                    folder+infile, 1, 'Mass', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True)
                if not dmonly:
                    particles.Mass *= ConvFactor

                ## Establish SubFind GrNr, key to searching over particle info per group... a lot of particles not included in Subhaloes as unbound fluff (etc)
                ## So every particles given GrNr of -1 and only those in a group are given the non-negative number, which we search for, reassign entire df using this
                ## then have to recreate the Offfset array as this should be just a running total of Length
                particles['GrNr'] = np.empty(len(particles.ParticleIDs), dtype=SUBoffset.dtype)
                particles.loc[:,'GrNr'] = -1
                # Create global index
                globalindex = np.arange(len(particles.ParticleIDs))
                index = np.concatenate([globalindex[OrigSUBoffset[GrNr]:OrigSUBoffset[GrNr]+OrigSUBlength[GrNr]] for GrNr in SUB_gtreslim])
                particles.loc[index,'GrNr'] = np.repeat(np.arange(NumSubGroups), OrigSUBlength[SUB_gtreslim])
                del(globalindex)
                
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                limlist = np.hstack(GrNrGroup.groups.viewvalues())
                size = GrNrGroup.size()
                if np.sum( size[size > SUBlength] ) > 0:
                    print "The Groups are not the correct size given by SUBlength"
                    print size[size > SUBlength]

                ## All important GroupBy FoF Group Number, if you leave particle list as is then can use SUBoffset and FOFoffset
                #GrNrGroup = particles.groupby('GrNr', sort=False)
                #alllist = np.hstack(GrNrGroup.groups.viewvalues())

                ## When outputting to final groups, such as the binary_group datafram, use the following to remove GrNr = -1
                #GrNrGroup = particles[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                ## Should be Bryan & Norman (ApJ 495, 80, 1998) virial mass [M_sol/h], except for SubFind subgroups are just M_total (like below)
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                binary_group = pd.DataFrame(GrNrGroup.Mass.sum().values).rename(columns={0: 'M_vir'})

                ## ID of most bound DM particle in structure, first DM particle in the subgroup
                ## Note this is different to Subhalo position which is min pot point
                binary_group['id_MBP'] = GrNrGroup.ParticleIDs.agg( lambda group: group.iget(0) ).astype(intsz)
                #  particles.ParticleIDs[SUBoffset].reset_index(0, drop=True).astype(intsz)
                
                ## Number of particles in the structure
                binary_group['n_particles'] = GrNrGroup.ParticleIDs.count().values
                #SUBlength

                ## MBP_Position Comoving Coordinates [Mpc/h]
                binary_group = binary_group.join(\
                    GrNrGroup[('PosX','PosY','PosZ')].agg( lambda group: group.iget(0) ).\
                    rename(columns={'PosX': 'position_MBP_X','PosY': 'position_MBP_Y','PosZ': 'position_MBP_Z'}))
 
                if debug:
                    print "SubFind"
                    print SUBoffset[0:4]
                    print particles.loc[0:4,('PosX','PosY','PosZ')].reset_index(0, drop=True)
                    print binary_group.loc[0:4,['position_MBP_X','position_MBP_Y','position_MBP_Z']]

                ## MBP Velocity proper [km/s]
                binary_group = binary_group.join(\
                    GrNrGroup[('VelX','VelY','VelZ')].agg( lambda group: group.iget(0) ).\
                    rename(columns={'VelX': 'velocity_MBP_X','VelY': 'velocity_MBP_Y','VelZ': 'velocity_MBP_Z'}))

                ## Calculate the COM_Position (that's relative to the MBD position) checking for periodic boundary conditions
                ## then add back the MBD position in and we should have the true COM position, checking for periodic BC again. 
                ## units are comoving [Mpc/h]
                ## CoM Position comoving [Mpc/h] - Recalculate it with only the DM particles
                particles['Radius'] = np.zeros(len(particles.ParticleIDs), dtype=particles['PosX'].iloc[0].dtype)
                for coord in ( 'X', 'Y', 'Z' ):
                    if not silent:
                        print "solve for coordinate "+coord
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    particles['Offset_'+coord] = GrNrGroup['Pos'+coord].transform( lambda group: group.iget(0) )
                    particles['Offset_'+coord] = particles['Pos'+coord] - particles['Offset_'+coord]
                    particles.loc[particles['Offset_'+coord] > HalfBoxSize, 'Offset_'+coord] -= BoxSize
                    particles.loc[particles['Offset_'+coord] < NegHalfBoxSize, 'Offset_'+coord] += BoxSize

                    # Working with groups so ignore GrNr < 0
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    # Start Radius calculation here using Offset_X, Offset_Y and Offset_Z
                    particles['Radius'] += GrNrGroup['Offset_'+coord].transform( np.square )

                    # Mass weighted Position, relative to the Most Bound Particle 
                    particles['WeightOffset_'+coord] = particles['Offset_'+coord] * particles['Mass']
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    binary_group['position_COM_'+coord] = GrNrGroup['WeightOffset_'+coord].sum().values / binary_group.M_vir
                    particles = particles.drop('WeightOffset_'+coord, axis=1)

                    # Add the Most Bound Position Back to the COM position
                    binary_group.loc[:,'position_COM_'+coord] += binary_group.loc[:,'position_MBP_'+coord]
                    # Test for Periodic Boundaries (note that it's not half boxsize but full!)
                    binary_group.loc[binary_group['position_COM_'+coord] < 0.,'position_COM_'+coord] += BoxSize
                    binary_group.loc[binary_group['position_COM_'+coord] > BoxSize,'position_COM_'+coord] -= BoxSize

                    ## Get the mass weighted CoM Velocity, already proper [km/s]
                    particles['WeightOffset_'+coord] = particles['Vel'+coord] * particles['Mass']
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    binary_group['velocity_COM_'+coord] = GrNrGroup['WeightOffset_'+coord].sum().values / binary_group.M_vir
                    particles = particles.drop('WeightOffset_'+coord, axis=1)
                                      
                    ## Make proper units
                    particles['Offset_'+coord] *= scalefactor 
                    ## Work out Sigma_v, the total 1D velocity dispersion, involves calculating the velocity difference per x/y/z direction
                    ## relative to the Centre of Mass 'bulk' velocity, then adding in a Hubble Flow component (H(z) * radius from Centre of Mass)
                    
                    ## Assign first particle velocity to be the COM velocity then replicate across the group using transform
                    particles['VelOffset_'+coord] = np.zeros(len(particles.ParticleIDs), dtype=particles['Offset_'+coord].iloc[0].dtype)
                    particles.loc[limlist,'VelOffset_'+coord] = np.repeat(binary_group['velocity_COM_'+coord].values, GrNrGroup['Vel'+coord].count().values)
                    ## Regroup, replicate
                    ## Add the Hubble flow to this particle velocity offset
                    particles['VelOffset_'+coord] = particles['Vel'+coord] - particles['VelOffset_'+coord] + H_of_z * particles['Offset_'+coord]

                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                particles['Radius'] = GrNrGroup.Radius.transform( np.sqrt )
                particles['Radius'] *= scalefactor

                ## Now perform the sort and cumulative mass summation / radius to get Vcirc
                get_vcirc = lambda GrNrGroup: calculatevcirc(GrNrGroup['Radius'], GrNrGroup['Mass'], Gunit=NewtonG_kmssq_MpcMsol)
                
                particles['Vcirc'] = np.zeros(len(particles.ParticleIDs), dtype=particles['VelX'].iloc[0].dtype)
                ## Assign to the correct points
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                particles.loc[limlist,'Vcirc'] = np.hstack( GrNrGroup.apply( get_vcirc ).values )

                ## R_halo [Mpc/h] furthest particle from the Most Bound Particle, already converted to proper
                binary_group['R_halo'] = GrNrGroup.Radius.max()
                ## Virial Radius [Mpc/h]
                binary_group['R_vir'] = binary_group['R_halo']#.copy()

                ## Now select out the groups that aren't actually tracking the GrNr > -1 numbering
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                ## Get max(Vcirc) i.e. V_max reached at R_max (already in proper units)
                idx = GrNrGroup.Vcirc.agg( np.argmax ).astype(intsz)

                binary_group['R_max'] = particles.loc[idx.values,'Radius'].values
                binary_group['V_max'] = particles.loc[idx.values,'Vcirc'].values

                if debug:
                    nansearch = np.sum(binary_group.V_max.apply(np.isnan))
                    if nansearch > 0:
                        print "Number NANs is ",nansearch, " in binary_group Vmax "
                    nansearch = np.sum(binary_group.R_max.apply(np.isnan))
                    if nansearch > 0:
                        print "Number NANs is ",nansearch, " in binary_group Rmax "

                if debug:
                    print "SubFind "
                    for testgrnr in np.arange(0,5):
                        partlist = particles['GrNr'] == testgrnr
                        sigma_v_test = np.sqrt( (np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_X']**2. ) + \
                                         np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_Y']**2. ) + \
                                         np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_Z']**2. ) ) 
                                         / (3. * np.sum(particles.loc[partlist,'Mass'])) )
                        print "testgrnr is ", testgrnr, sigma_v_test
                        print "ID ", binary_group.loc[testgrnr,'id_MBP']
                        print binary_group.loc[testgrnr,'velocity_COM_X'],binary_group.loc[testgrnr,'velocity_COM_Y'],binary_group.loc[testgrnr,'velocity_COM_Z']
                        print binary_group.loc[testgrnr,'position_COM_X'],binary_group.loc[testgrnr,'position_COM_Y'],binary_group.loc[testgrnr,'position_COM_Z']
                        print binary_group.loc[testgrnr,'position_MBP_X'],binary_group.loc[testgrnr,'position_MBP_Y'],binary_group.loc[testgrnr,'position_MBP_Z']

                ## Add the particles velocity, relative to the CoM and with Hubble Flow, in quadrature with mass weighting
                particles['sigma_v'] = particles.Mass  * (particles['VelOffset_X']**2. + particles['VelOffset_Y']**2. + particles['VelOffset_Z']**2.)
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                binary_group['sigma_v'] = np.sqrt(GrNrGroup['sigma_v'].sum().values / (3. * binary_group.M_vir))
                particles = particles.drop('sigma_v', axis=1)
                 
                for coord1, coord2, coord3 in ( ('X', 'Z', 'Y'), ('Y', 'X', 'Z'), ('Z', 'Y', 'X')): 
                    particles['temp'] = particles.Mass  * ( particles['VelOffset_'+coord2]*particles['Offset_'+coord3] - particles['VelOffset_'+coord3]*particles['Offset_'+coord2] )
                    ## Now work out spin, the specific angular momentum vector [Mpc/h * km/s]
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                    binary_group['spin_'+coord1] = np.sqrt(GrNrGroup['temp'].sum().values / binary_group.M_vir)
                    particles = particles.drop('temp', axis=1)

                ## Just put a dummy variable here for triaxial shape, q, given as b/a
                binary_group['q_triaxial'] = np.zeros(NumSubGroups, dtype=binary_group.sigma_v.dtype)
                ## Just put a dummy variable here for triaxial shape, s, given as c/a
                binary_group['s_triaxial'] = np.zeros(NumSubGroups, dtype=binary_group.sigma_v.dtype)

                ## Close particles dataframe before loading anything more into memory, leave as None, so that we don't have to reread datafiles for FOF
                if closeparticles:
                    del(particles)

                ## Manipulate the dataframe to fill the Tiamat struct and create the neccessary binary formatted output
                BINARY_TIAMAT_DATASET = np.empty(NumSubGroups, dtype=BINARY_TIAMAT_DATATYPE)
                for key, size in BINARY_TIAMAT_DATAFORMAT.iteritems():
                    if key == 'shape_eigen_vectors' or key == 'padding':
                        tmp = 1
                    else:
                        if len(BINARY_TIAMAT_DATASET[key].shape) > 1:
                            BINARY_TIAMAT_DATASET[key] = binary_group[[key+'_X', key+'_Y', key+'_Z']]#.astype(size)
                        else:
                            BINARY_TIAMAT_DATASET[key] = binary_group[key].astype(size)

                ## now output the struct
                BINARY_TIAMAT_DATASET.tofile(fout)

                ## Close binary_group dataframe
                del(binary_group)
        if not silent:
            print "Wrote SubFind Properties file"

#########################################################
##
## Create the FOF Group Catalogue File
##
#########################################################
        if not silent:
            print "Now FOF Group Info "

        if subdivide:
            ## Create a directory (if needed) and copy subfivided files out
            out_directory = outfolder+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups.'+str(subfile)
        else:
            if subfile > 1:
                print "Warning: Can't append multiple subfiles to the same Tiamat single file yet"
            ## Create a single file, with the name of the subdived head directory, to hold all info 
            out_directory = outfolder+'halos/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)            
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups'

        ## Write the FOF Catalog
        print "Write the FOF catalog_groups"
        if not silent:
            print "Writing to "+particles_outfile
        if NumFoFGroups > 0:
            if NumFoFGroups != len(FOFlength):
                print "The FOF groups is ", NumFoFGroups, " while the length of FOFlength is ",len(FOFlength)
            if NumFoFGroups != len(FOFoffset):
                print "The FOF groups is ", NumFoFGroups, " while the length of FOFoffset is ",len(FOFoffset)
        if NumSubGroups > 0:
            if NumSubGroups != np.sum(NsubPerHalo):
                print "The number of subhaloes is ", NumSubGroups, " while the total outputted in NsubPerHalo ",np.sum(NsubPerHalo)           
        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving N_groups, the number of FoF groups in the file
            fout.write(struct.pack('i', NumFoFGroups))
            ## write 4-byte integer giving the number of bytes used for the group particle offsets (N_b_go)
            fout.write(struct.pack('i', bytesz))
            ## Write Body, 3-arrays, each N_b_go long
            ## write the Length array
            if NumFoFGroups > 0:
                fout.write(FOFlength.astype(uint32)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 
            ## write the Offset array
            if NumFoFGroups > 0:
                fout.write(FOFoffset.astype(intsz)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 

            ## write the SubGroup Number of Files
            if NumFoFGroups > 0:
                fout.write(NsubPerHalo.astype(uint32)) 
            else:
                fout.write(np.array([0]).astype(uint32))                 

        if not silent:
            print "Wrote FOF Group file"


#########################################################
##
## Create the FOF Group Properties File
##
#########################################################
        if not silent:
            print "Now FOF Properties Info "
        if subdivide:
            ## Create a directory (if needed) and copy subfivided files out
            out_directory = outfolder+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups_properties/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups_properties.'+str(subfile)
        else:
            if subfile > 1:
                print "Warning: Can't append multiple subfiles to the same Tiamat single file yet"
            ## Create a single file, with the name of the subdived head directory, to hold all info 
            out_directory = outfolder+'catalogs/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_groups_properties'

        ## Write the FOF Catalog
        print "Write the FOF catalog_groups_properties"
        if not silent:
            print "Writing to "+particles_outfile

        if NumFoFGroups > 0:
            if NumFoFGroups != len(FOFlength):
                print "The FOF length is ", len(FOFlength), " nut the Number of FOF groups output is ",NumFoFGroups

        with open(particles_outfile, "wb") as fout:
            ## write 4-byte integer giving i_sub, the number of this file in the list of n_sub subfiles
            fout.write(struct.pack('i', subfile)) 
            ## write 4-byte integer giving n_sub, the number of subfiles in the directory
            fout.write(struct.pack('i', numfile)) 
            ## write 4-byte integer giving i_groups, the number of FoF groups in this file
            fout.write(struct.pack('i', NumFoFGroups))
            ## write 4-byte integer giving n_groups, the number of FoF groups in all files
            fout.write(struct.pack('i', TotNumFoFGroups))

#########################################################
##
## Have to calculate the FOF Group Properties 
##
#########################################################
            if NumFoFGroups > 0:
                ## Now read in the ParticleIDs
                if closeparticles:
                    particles=pyread_gadget_hdf5(folder+infile, 1, 'ParticleIDs', noloop=noloop, sub_dir='FoF', silent=silent).rename(columns={0: 'ParticleIDs'})
                if dmonly:
                    if not silent:
                        print "There's only DM so don't change ParticleIDs"
                else:
                    if not silent:
                        print "Reset the DM ParticleIDs, removing the gas count "
                    particles.ParticleIDs -= npart_name

                if closeparticles:
                ## Get positions in Mpc/h COMOVING
                    particles=particles.join( \
                        pyread_gadget_hdf5(folder+infile, 1, 'Coordinates', noloop=noloop, sub_dir='FoF', silent=silent).\
                        rename(columns={0:'PosX', 1:'PosY', 2:'PosZ'}) )

                    ## Get velocities in km/s proper
                    particles=particles.join( \
                        pyread_gadget_hdf5(folder+infile, 1, 'Velocity', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True).\
                        rename(columns={0:'VelX', 1:'VelY', 2:'VelZ'}) )

                    ## Get Mass in Msol/h and correct for missing baryon particles (if neccessary)
                    particles['Mass']=pyread_gadget_hdf5(folder+infile, 1, 'Mass', noloop=noloop, sub_dir='FoF', silent=silent, physunits=True, leaveh=True)
                    if not dmonly:
                        particles.Mass *= ConvFactor

                ## Establish FoF GrNr, key to searching over particle info per group, set particles to -1 if not in group
                particles['GrNr'] = np.empty(len(particles.ParticleIDs), dtype=FOFoffset.dtype)
                particles.loc[:,'GrNr'] = -1 
                # Create global index
                globalindex = np.arange(len(particles.ParticleIDs))
                index = np.concatenate([globalindex[OrigFOFoffset[GrNr]:OrigFOFoffset[GrNr]+OrigFOFlength[GrNr]] for GrNr in FOF_gtreslim])
                particles.loc[index,'GrNr'] = np.repeat(np.arange(NumFoFGroups), OrigFOFlength[FOF_gtreslim])
                del(globalindex)
                
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                limlist = np.hstack(GrNrGroup.groups.viewvalues())

                ## All important GroupBy FoF Group Number, if you leave particle list as is then can use SUBoffset and FOFoffset
                #GrNrGroup = particles.groupby('GrNr', sort=False)
                #alllist = np.hstack(GrNrGroup.groups.viewvalues())

                ## When outputting to final groups, such as the binary_group datafram, use the following to remove GrNr = -1
                #GrNrGroup = particles[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                ## Bryan & Norman (ApJ 495, 80, 1998) virial mass 'proper' for what that's worth [M_sol/h]
                binary_group = pyread_gadget_hdf5(\
                    folder+infile, 10, 'Halo_M_TopHat200', noloop=noloop, sub_dir='SubFind', silent=silent, physunits=True, leaveh=True).\
                rename(columns={0: 'M_vir'})
               ## Virial Radius proper [Mpc/h]
                binary_group['R_vir'] = pyread_gadget_hdf5(\
                    folder+infile, 10, 'Halo_R_TopHat200', noloop=noloop, sub_dir='SubFind', silent=silent, physunits=True, leaveh=True)

                ## Now create the output binary_group size
                binary_group = binary_group.loc[FOF_gtreslim].reset_index(0, drop=True)

                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                if debug:
                    if len(FOF_gtreslim) != len(GrNrGroup):
                        print len(FOF_gtreslim), len(GrNrGroup)

                ## ID of most bound DM particle in structure, first DM particle in the subgroup
                ## Note this is different to Subhalo position which is min pot point
                binary_group['id_MBP'] = GrNrGroup.ParticleIDs.agg( lambda group: group.iget(0) ).astype(intsz)
                #  particles.ParticleIDs[SUBoffset].reset_index(0, drop=True).astype(intsz)
                
                ## Number of particles in the structure
                binary_group['n_particles'] = GrNrGroup.ParticleIDs.count().values

                ## MBP_Position Comoving Coordinates [Mpc/h]
                binary_group = binary_group.join(\
                    GrNrGroup[('PosX','PosY','PosZ')].agg( lambda group: group.iget(0) ).\
                    rename(columns={'PosX': 'position_MBP_X','PosY': 'position_MBP_Y','PosZ': 'position_MBP_Z'}))
 
                if debug:
                    print "FOF"
                    print FOFoffset[0:4]
                    print particles.loc[0:4,('PosX','PosY','PosZ')].reset_index(0, drop=True)
                    print binary_group.loc[0:4,['position_MBP_X','position_MBP_Y','position_MBP_Z']]

                ## MBP Velocity proper [km/s]
                binary_group = binary_group.join(\
                    GrNrGroup[('VelX','VelY','VelZ')].agg( lambda group: group.iget(0) ).\
                    rename(columns={'VelX': 'velocity_MBP_X','VelY': 'velocity_MBP_Y','VelZ': 'velocity_MBP_Z'}))

                ## Calculate the COM_Position (that's relative to the MBD position) checking for periodic boundary conditions
                ## then add back the MBD position in and we should have the true COM position, checking for periodic BC again. 
                ## units are comoving [Mpc/h]
                ## CoM Position comoving [Mpc/h] - Recalculate it with only the DM particles
                particles.loc[:,'Radius'] = 0.#np.zeros(len(particles.ParticleIDs), dtype=particles['PosX'].iloc[0].dtype)
                for coord in ( 'X', 'Y', 'Z' ):
                    if not silent:
                        print "solve for coordinate "+coord
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    particles['Offset_'+coord] = GrNrGroup['Pos'+coord].transform( lambda group: group.iget(0) )                    
                    particles['Offset_'+coord] = particles['Pos'+coord] - particles['Offset_'+coord]
                    particles.loc[particles['Offset_'+coord] > HalfBoxSize, 'Offset_'+coord] -= BoxSize
                    particles.loc[particles['Offset_'+coord] < NegHalfBoxSize, 'Offset_'+coord] += BoxSize

                    # Working with groups so ignore GrNr < 0
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    # Start Radius calculation here using Offset_X, Offset_Y and Offset_Z
                    particles['Radius'] += GrNrGroup['Offset_'+coord].transform( np.square )

                    # Mass weighted Position, relative to the Most Bound Particle 
                    particles['WeightOffset_'+coord] = particles['Offset_'+coord] * particles['Mass']
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    binary_group['position_COM_'+coord] = GrNrGroup['WeightOffset_'+coord].sum().values / binary_group.M_vir
                    particles = particles.drop('WeightOffset_'+coord, axis=1)
                                                          
                    # Add the Most Bound Position Back to the COM position
                    binary_group.loc[:,'position_COM_'+coord] += binary_group.loc[:,'position_MBP_'+coord]
                    # Test for Periodic Boundaries (note that it's not half boxsize but full!)
                    binary_group.loc[binary_group['position_COM_'+coord] < 0.,'position_COM_'+coord] += BoxSize
                    binary_group.loc[binary_group['position_COM_'+coord] > BoxSize,'position_COM_'+coord] -= BoxSize

                    ## Get the mass weighted CoM Velocity, already proper [km/s]
                    particles['WeightOffset_'+coord] = particles['Vel'+coord] * particles['Mass']
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                    binary_group['velocity_COM_'+coord] = GrNrGroup['WeightOffset_'+coord].sum().values / binary_group.M_vir
                    particles = particles.drop('WeightOffset_'+coord, axis=1)
                                                          
                    ## Make proper units
                    particles['Offset_'+coord] *= scalefactor 
                    ## Work out Sigma_v, the total 1D velocity dispersion, involves calculating the velocity difference per x/y/z direction
                    ## relative to the Centre of Mass 'bulk' velocity, then adding in a Hubble Flow component (H(z) * radius from Centre of Mass)
                    
                    ## Assign first particle velocity to be the COM velocity then replicate across the group using transform
                    particles['VelOffset_'+coord] = np.zeros(len(particles.ParticleIDs), dtype=particles['Offset_'+coord].iloc[0].dtype)
                    particles.loc[limlist,'VelOffset_'+coord] = np.repeat(binary_group['velocity_COM_'+coord].values, GrNrGroup['Vel'+coord].count().values)
                    ## Regroup, replicate
                    ## Add the Hubble flow to this particle velocity offset
                    particles['VelOffset_'+coord] = particles['Vel'+coord] - particles['VelOffset_'+coord] + H_of_z * particles['Offset_'+coord]

                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                particles['Radius'] = GrNrGroup.Radius.transform( np.sqrt )
                particles['Radius'] *= scalefactor

                ## Now perform the sort and cumulative mass summation / radius to get Vcirc
                get_vcirc = lambda GrNrGroup: calculatevcirc(GrNrGroup['Radius'], GrNrGroup['Mass'], Gunit=NewtonG_kmssq_MpcMsol)
                
                particles['Vcirc'] = np.zeros(len(particles.ParticleIDs), dtype=particles['VelX'].iloc[0].dtype)
                ## Assign to the correct points
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                particles.loc[limlist,'Vcirc'] = np.hstack( GrNrGroup.apply( get_vcirc ).values )

                ## R_halo [Mpc/h] furthest particle from the Most Bound Particle, already converted to proper
                binary_group['R_halo'] = GrNrGroup.Radius.max()

                ## Now select out the groups that aren't actually tracking the GrNr > -1 numbering
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)

                ## Get max(Vcirc) i.e. V_max reached at R_max (already in proper units)
                idx = GrNrGroup.Vcirc.agg( np.argmax ).astype(intsz)

                binary_group['R_max'] = particles.loc[idx.values,'Radius'].values
                binary_group['V_max'] = particles.loc[idx.values,'Vcirc'].values

                if debug:
                    nansearch = np.sum(binary_group.V_max.apply(np.isnan))
                    if nansearch > 0:
                        print "Number NANs is ",nansearch, " in binary_group Vmax "
                    nansearch = np.sum(binary_group.R_max.apply(np.isnan))
                    if nansearch > 0:
                        print "Number NANs is ",nansearch, " in binary_group Rmax "

                if debug:
                    print "FOF "
                    for testgrnr in np.arange(0,5):
                        partlist = particles['GrNr'] == testgrnr
                        sigma_v_test = np.sqrt( (np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_X']**2. ) + \
                                         np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_Y']**2. ) + \
                                         np.sum( particles.loc[partlist,'Mass'] * particles.loc[partlist,'VelOffset_Z']**2. ) ) 
                                         / (3. * np.sum(particles.loc[partlist,'Mass'])) )
                        print "testgrnr is ", testgrnr, sigma_v_test
                        print "ID ", binary_group.loc[testgrnr,'id_MBP']
                        print binary_group.loc[testgrnr,'velocity_COM_X'],binary_group.loc[testgrnr,'velocity_COM_Y'],binary_group.loc[testgrnr,'velocity_COM_Z']
                        print binary_group.loc[testgrnr,'position_COM_X'],binary_group.loc[testgrnr,'position_COM_Y'],binary_group.loc[testgrnr,'position_COM_Z']
                        print binary_group.loc[testgrnr,'position_MBP_X'],binary_group.loc[testgrnr,'position_MBP_Y'],binary_group.loc[testgrnr,'position_MBP_Z']

                # Calculate sigma_v
                particles['sigma_v'] = particles.Mass  * (particles['VelOffset_X']**2. + particles['VelOffset_Y']**2. + particles['VelOffset_Z']**2.)
                GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                binary_group['sigma_v'] = np.sqrt(GrNrGroup['sigma_v'].sum().values / (3. * binary_group.M_vir))
                particles = particles.drop('sigma_v', axis=1)

                for coord1, coord2, coord3 in ( ('X', 'Z', 'Y'), ('Y', 'X', 'Z'), ('Z', 'Y', 'X')): 
                    particles['temp'] = particles.Mass  * ( particles['VelOffset_'+coord2]*particles['Offset_'+coord3] - particles['VelOffset_'+coord3]*particles['Offset_'+coord2] )
                    ## Now work out spin, the specific angular momentum vector [Mpc/h * km/s]
                    GrNrGroup = particles.loc[particles['GrNr'] > -1].groupby('GrNr', sort=False)
                    binary_group['spin_'+coord1] = np.sqrt(GrNrGroup['temp'].sum().values / binary_group.M_vir)
                    particles = particles.drop('temp', axis=1)

                ## Just put a dummy variable here for triaxial shape, q, given as b/a
                binary_group['q_triaxial'] = np.zeros(NumFoFGroups, dtype=binary_group.sigma_v.dtype)
                ## Just put a dummy variable here for triaxial shape, s, given as c/a
                binary_group['s_triaxial'] = np.zeros(NumFoFGroups, dtype=binary_group.sigma_v.dtype)

                ## Close particles dataframe before loading anything more into memory
                ParticleIDs = np.array(particles.loc[limlist,'ParticleIDs']).flatten()

                del(particles)

                ## Manipulate the dataframe to fill the Tiamat struct and create the neccessary binary formatted output
                BINARY_TIAMAT_DATASET = np.empty(NumFoFGroups, dtype=BINARY_TIAMAT_DATATYPE)
                for key, size in BINARY_TIAMAT_DATAFORMAT.iteritems():
                    if key == 'shape_eigen_vectors' or key == 'padding':
                        tmp = 1
                    else:
                        if len(BINARY_TIAMAT_DATASET[key].shape) > 1:
                            BINARY_TIAMAT_DATASET[key] = binary_group[[key+'_X', key+'_Y', key+'_Z']]#.astype(size)
                        else:
                            BINARY_TIAMAT_DATASET[key] = binary_group[key].astype(size)

                ## now output the struct
                BINARY_TIAMAT_DATASET.tofile(fout)

#########################################################
##
## Create the ParticleID Catalogue File
##
#########################################################

        if subdivide:
            ## Create a directory (if needed) and copy subfivided files out
            out_directory = outfolder+'subfind_'+str(snapnum).zfill(3)+'.catalog_particles/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_particles.'+str(subfile)
        else:
            if subfile > 1:
                print "Warning: Can't append multiple subfiles to the same Tiamat single file yet"
            ## Create a single file, with the name of the subdived head directory, to hold all info 
            out_directory = outfolder+'halos/'
            if not os.path.exists(out_directory):
                if not silent:
                    print "Create Directory ", out_directory
                os.makedirs(out_directory)
            particles_outfile = out_directory+'subfind_'+str(snapnum).zfill(3)+'.catalog_particles'

        ## Use the modified ParticleIDs created above, which has already deleted the Baryon IDs

        ## Write the Particle Catalog
        print "Write the entire catalog_particles"

        if not silent:
            print "Writing to "+particles_outfile
        with open(particles_outfile, "wb") as fout:        
            ## write 4-byte integer that gives N_b, the byte-length of the particle IDs in the file
            fout.write(struct.pack('i', bytesz)) 
            if not silent:
                print "byte-length of IDs ",bytesz
            if TotNumFoFGroups > 0:
                ## unsigned integer of length N_b giving N_p, the number of particles in the file
                fout.write(struct.pack('i', len(ParticleIDs)))
                if not silent:
                    print "number of particles ",len(ParticleIDs)
                ## write the Particle IDs
                fout.write(ParticleIDs.astype(intsz))
            else:
                ## unsigned integer of length N_b giving N_p, the number of particles in the file
                fout.write(struct.pack('i', 0))
                ## write the Particle IDs
                fout.write(np.array([0]).astype(intsz))
        if not silent:
            print "Wrote Particle file"

    #from IPython import embed
    #embed()
    #1/0
