#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import argparse
import subprocess
import sys

from IPython import embed
from collections import Counter

'''
	NAME:
		plot_cpu_usage.py

	PURPOSE:
		This code reads in cpu.txt files from Gadget2/3 and plots the various components.

	MANUAL:
		python plot_cpu_usage.py -h

	CALLING SEQUENCE:
		python plot_cpu_usage.py CPUFILE MAKEFILE [--gadget3, --non_cosmo, --ymax YMAX, 
								--noconstrain_x, --dmonly, -o OUTFILE, --silence]

	INPUTS:
		CPUFILE: 		Name of the cpu file to read in.
		MAKEFILE:		Name of the Makefile used to compile the executable,
						which is parsed to retrieve the value of PMGRID."

	OPTIONAL INPUTS:
		--ymax float:			Upper limit of the y-axis. Defaults to 110% 
								of the actual max, but it is often useful to 
								specify a fixed value to enable direct 
								comparison between two runs.
		-o/--outfile string:	The output file name. By default, the function
								outputs cpu.png in the same folder as the 
								cpufile.

	OPTIONAL FLAGS:
		--gadget3:		The format of cpu.txt produced by public Gadget2 or 
						Gadget3 (if used) is quite different.
		--non_cosmo:	By default the time variable is considered to be the 
						cosmological expansion factor; as such the function 
						adds an upper axis for redshift. If use, the time 
						variable is converted from internal units to Myr 
						and the upper axis is omitted.
		--noconstrain_x:By default, the time axis is constrained by the max 
						and min values. If used, there will be no constraint.
		--dmonly:		If use, the function does not read in the SPH values.
		--silence:		If use, no detailed information will be shown.

	OUTPUTS:
		a figure saved as OUTFILE

	
	MODIFICATION HISTORY:

		Yuxiang Qin, http://www.ph.unimelb.edu.au/~yuxiangq/
		[06/04/16] Converted and simplfied the IDL script plot_cpu_usage.pro from Rob Crain

	Please feel free to contact me :)
'''

############################ construct the manual and arguments ############################
parser = argparse.ArgumentParser(
		description='''This code reads in the cpu.txt file from Gadget2/3
						and plots the various components. Feel free to contact
						me (Yuxiang Qin, http://www.ph.unimelb.edu.au/~yuxiangq/)''',
        usage="plot_cpu_usage.py cpufile makefile",
        epilog="=============================================================")

parser.add_argument(dest="cpufile", type=file, metavar='cpufile string',
        help="Name of the cpu file to read in.")

parser.add_argument(dest="makefile", type=file, metavar='makefile string',
        help="Name of the Makefile used to compile the executable, which is parsed to retrieve the value of PMGRID.")

parser.add_argument("--gadget3", dest="gadget3", default=False, action='store_true',
        help="The format of cpu.txt produced by public Gadget2 or Gadget3 (if uses) is quite different.")

parser.add_argument("--non_cosmo", dest="non_cosmo", default=False, action='store_true',
        help="By default the time variable is considered to be the cosmological expansion factor; as such the function adds an upper axis for redshift. If use, the time variable is converted from internal units to Myr and the upper axis is omitted.")

parser.add_argument("--ymax", dest="ymax", metavar='float', type=float,
        help="Upper limit of the y-axis. Defaults to 110%% of the actual max, but it is often useful to specify a fixed value to enable direct comparison between two runs.")

parser.add_argument("--noconstrain_x", dest="noconstrain_x", default=False, action='store_true',
        help="By default the time axis is constrained by the max and min values. If used, there will be no constraint.")

parser.add_argument("--dmonly", dest="dmonly", default=False, action='store_true',
        help="If true, then function does not read in the SPH values.")

parser.add_argument('-o','--outfile', dest="outfile", type=str, metavar='string',
        help="The output file name. By default, the function outputs cpu.png in the same folder as the cpufile.")

parser.add_argument("--silence", dest="silence", default=False, action='store_true',
        help="If true, no detailed information will be shown.")

args = parser.parse_args()


############################  START  ############################
if args.gadget3:
    if not args.silence:
        print '[INFO:] This is a Gadget3 cpu file.'
    gadget = 'Gadget3-BG'
else:
    if not args.silence:
        print '[INFO:] This is a Gadget2 cpu file.'
    gadget = 'Gadget2'

# get size of pmgrid from Makefile
pmstatement = subprocess.Popen('grep DPMGRID %s'%args.makefile.name, stdout=subprocess.PIPE,shell=True).communicate()[0]

# test whether -DPMGRID has been commented
if '#' in pmstatement:
    pmstatement = 'PMGRID unused'
    pmtrim = 0
    if not args.silence: print '[INFO:] ',pmstatement
else:
    pmstatement=  pmstatement[pmstatement.find('-'):-1]
    pmtrim = 2
    if not args.silence: print "[INFO:] found PMGRID: %s"%pmstatement

# find number of cpus
with open(args.cpufile.name) as f:
    strdata = f.read()
    data = strdata.split('\n')
embed()
#    data = f.readlines()

elements = ['Step','total', 'treegrav', 'treebuild', 'treeupdate', 'treewalk', 'treecomm', 'treeimbal', 'pmgrav', \
'sph', 'density', 'denscomm', 'densimbal', 'hydrofrc', 'hydcomm', 'hydimbal', 'hmaxupdate', 'domain', 'potential',
'predict', 'kicks', 'i/o', 'peano', 'sfrcool', 'cooling', 'sfr', 'enrichment', 'evolution', 'molecules', 'blackholes', \
'fof', 'lineofsight', 'binning', 'misc']

embed()
data = np.array(data)

if not args.silence: print "[INFO:] found %d complete time-steps"%(len(data)-1)
ncpu = int(data[0][data[0].find('CPUs')+6:-1])
if not args.silence: print "[INFO:] ncpu = %d\n[INFO:] extracting components..."%ncpu

num_names = 0
for item in elements:
    if item in data[0]: 
        num_names += 1
print num_names
embed()
# read the time array
time = np.array([item.split(',')[1].split(':')[1] for item in data[0::num_names+1]],dtype=np.float)
if args.non_cosmo: time*=500.0/0.372449 #convert internal units into Myr

# grep out the various elements
lines = [1, 1, 2, 2, 5, 6, 9, 9, 10, 11, 14, 8, 17]
elements = ['total', 'allother', 'treegrav', 'treeother', 'treewalk', 'treecomm', 'sph', 'sphother', 'density', 'denscomm', 'hydcomm', 'pmgrav', 'domain']
s = pd.Series([None]*len(elements), index=elements)

for line, element in zip(lines,elements):
    s[element] = np.array([item.split()[1] for item in data[line::num_names+1]], dtype=np.float)/3600#hours

# check that the gas is indeed missing
if args.dmonly and s['sph'][-1]>1e-3: 
    print "[INFO:] you've selected dmonly but the sph time is non-negligable at %.1f hours.\t[INFO:] EXIT"%s['sph'][-1]
    sys.exit(1)

# generate 'other' values
if args.dmonly:
    s['allother'] = s['total'] - s['treegrav'] - s['pmgrav'] - s['domain']
else:
    s['allother'] = s['total'] - s['treegrav'] - s['pmgrav'] - s['domain'] - s['sph']
    s['sphother'] = s['sph'] - s['hydcomm'] - s['denscomm'] - s['density']
    s['treeother'] = s['treegrav'] - s['treewalk'] - s['treecomm']

## find occasions where snapshot-based restarts have reset the wallclock, and add on the necessary offset.
#for i in range(len(s['total'])-2):
#    if s['total'][i+1] < s['total'][i]:
#        if not args.silence: print "[INFO:] adding offset to total of %.3f"%(s['total'][i]-s['total'][i+1])
#        for element in elements:
#            s[element][i+1:len(s[element])-1] += s[element][i] - s[element][i+1]

# edge_value
treewalk = s['treewalk']
treecomm = s['treecomm']+treewalk
treeother = s['treeother']+treecomm
treegrav = s['treegrav']

pmgrav = s['pmgrav']+treegrav

density = s['density']+pmgrav
denscomm = s['denscomm']+density
hydcomm = s['hydcomm']+denscomm
sphother = s['sphother']+hydcomm
sph = s['sph']+pmgrav

domain = s['domain']+sph
allother = s['allother']+domain
total = s['total']

# plot
fig,ax=plt.subplots(1,1,figsize=(8,8))

ax.plot(time,total,'k-',label='Total',lw=3)
ax.fill_between(time,domain,allother,facecolor='#1b9e77',lw=3)
ax.fill_between(time,sph,domain,facecolor='#d95f02',lw=3)
ax.fill_between(time,pmgrav,sph,facecolor='#7570b3',lw=3)
ax.plot(time,sphother,'k--',lw=1)
ax.plot(time,hydcomm,'k--',lw=1)
ax.plot(time,denscomm,'k--',lw=1)
ax.plot(time,density,'k--',lw=1)
ax.fill_between(time,treegrav, pmgrav,facecolor='#e7298a',lw=3)
ax.fill_between(time,[0]*len(time),treegrav,facecolor='#66a61e',lw=3)
ax.plot(time,treeother,'k--',lw=1)
ax.plot(time,treecomm,'k--',lw=1)
ax.plot(time,treewalk,'k--',lw=1)

# for legend 
ax.plot(time,[-1]*len(time),color='#1b9e77',label='Other',lw=5)
ax.plot(time,[-1]*len(time),color='#d95f02',label='Domain',lw=5)
ax.plot(time,[-1]*len(time),color='#7570b3',label='SPH',lw=5)
ax.plot(time,[-1]*len(time),label='  Other',lw=0)
ax.plot(time,[-1]*len(time),label='  Hydro comms',lw=0)
ax.plot(time,[-1]*len(time),label='  Density comms',lw=0)
ax.plot(time,[-1]*len(time),label='  Density calc',lw=0)
ax.plot(time,[-1]*len(time),color='#e7298a',label='PM gravity',lw=5)
ax.plot(time,[-1]*len(time),color='#66a61e',label='Tree gravity',lw=5)
ax.plot(time,[-1]*len(time),label='  Other',lw=0)
ax.plot(time,[-1]*len(time),label='  Tree comm',lw=0)
ax.plot(time,[-1]*len(time),label='  Tree walk',lw=0)
ax.legend(loc='upper left',fontsize=13,frameon=False)

ax.text(0.40,0.92,args.cpufile.name[args.cpufile.name.find('Smaug')+6:args.cpufile.name.find('data')-1]+'\n'+gadget+'\n'+r'$N_{\rm CPU}=%d$'%ncpu+'\n'+pmstatement[2:], transform=ax.transAxes,fontsize=13,\
            horizontalalignment='left',verticalalignment='center')
if args.noconstrain_x is False: 
    ax.set_xlim(time[0],time[-1])
if args.ymax is None:
    ax.set_ylim(0,total[-1]*1.1)
else:
    ax.set_ylim(0,args.ymax)

ax.set_ylabel('Wallclock [hr]',fontsize=15)
ax.xaxis.set_tick_params(labelsize=15)
ax.yaxis.set_tick_params(labelsize=15)

if args.non_cosmo:
    ax.set_xlabel('Time [Myr]',fontsize=15)
else:
    ax.set_xlabel('Expansion Factor',fontsize=15)
    zs = np.array([75,30,20,10,9,8,7,6,5])
    # redshift axis
    axz = ax.twiny()
    axz.set_xlim(ax.get_xlim())
    axz.set_xticks(1./(1.+zs))
    axz.set_xticklabels(zs)
    axz.set_xlabel('Redshift',fontsize=15)
    axz.xaxis.set_tick_params(labelsize=15)

plt.tight_layout()
if args.outfile is None:
    outfile = args.cpufile.name[:args.cpufile.name.find('cpu.txt')]+'cpu.png'
    plt.savefig(outfile)
    if not args.silence:
        print "[INFO:] saved into %s"%outfile
else: 
    plt.savefig(args.outfile)
    if not args.silence:
        print "[INFO:] saved into %s"%args.outfile
