import dispersion
import numpy as np

filein = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/snapshot_045/snap_045'

res = 25.
CalcVal = 'DM'
LoSVal = 'Int_Z'
outdir = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/render/'
#dispersion.phasediagram(filein,outdir)

outdir = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/render/'+"{0:.3f}".format( res )+'kpc/'
dispersion.calcrender(filein,outdir,ndlos=1,num_threads=4,res=res,CalcVal=CalcVal,LoSVal=LoSVal, ProjCoord='XY')#zoom=20., dlos = 0.02
dispersion.calcrender(filein,outdir,ndlos=1,num_threads=4,res=res,CalcVal=CalcVal,LoSVal=LoSVal, ProjCoord='XZ')
dispersion.calcrender(filein,outdir,ndlos=1,num_threads=4,res=res,CalcVal=CalcVal,LoSVal=LoSVal, ProjCoord='YZ')
#dispersion.plotrender(outdir+'Render_'+"{0:.3f}".format( res )+'kpc_'+CalcVal+'_'+LoSVal+'.npz',outdir,CalcVal=CalcVal,LoSVal=LoSVal)

filearr = np.array([outdir+'Render_'+"{0:.3f}".format( res )+'kpc_'+'ProjXY_'+CalcVal+'_'+LoSVal+'.npz',
                    outdir+'Render_'+"{0:.3f}".format( res )+'kpc_'+'ProjXZ_'+CalcVal+'_'+LoSVal+'.npz',
                    outdir+'Render_'+"{0:.3f}".format( res )+'kpc_'+'ProjYZ_'+CalcVal+'_'+LoSVal+'.npz'])
legend = np.array(['XY proj z=0',
                   'XZ proj z=0',
                   'YZ proj z=0'])
outdir = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/render/'+"{0:.3f}".format( res )+'kpc/'
dispersion.multirender(filearr,legend,outdir=outdir,CalcVal=CalcVal,LoSVal=LoSVal)
