import numpy as np

def calcnion(filename, ion = 'all', mstar = 1e6, permstar = False, perbaryon = False, peratom = False):
    """ Read in Starburst99 quanta output and calculate number of Ionising photons

    Parameters
    ----------

    filename: the filename for the Starburst99 Quanta output; output option 1 (and ctrl-click 7 too) 
    from the following website:
    http://www.stsci.edu/science/starburst99/docs/parameters.html

    ion: Default is to add all the ionising photons, but user can specify HI, HeI or HeII to get number
    of ions released that ionised those states

    mstar: The stellar mass assumed by Starburst99, default is 1e6 Msol

    permstar: Boolean, If True returns the number of ionising photons per unit mass

    perbaryon: Boolean, If True returns the number of ionising photons per baryon 

    peratom: Boolean, If True returns the number of ionising photons per atom, assumes mean molecular weight for Zsol of 1/1.67 

    (note can't have both perbaryon and permstar, error will return -1)

    Notes
    -----

    Requires numpy
    Uses numpy.trapz tabular integration which could be improved accuracy-wise

    by
    Alan Duffy, 3/10/14 
    Queries to twitter... @astroduff  
    or email... mail@alanrduffy.com

    """

    # Test output normalisation
    output = 0
    if permstar != False:
        output += 1
    if perbaryon != False:
        output += 1
    if peratom != False:
        output += 1      
    if output > 1:
        print "Error: Can't select ion count to be relative to more than either per baryon, per stellar mass or per atom. Choose only one. "
        return -1

    ## Read in the Starburst 99 output file '###.quanta1'
    t, HI, HeI, HeII = np.loadtxt(filename,skiprows=7,usecols=(0,1,3,5), unpack=True)

    ## The ionised number of atoms per second
    if ion.lower() == 'all':
        Ion_rate = 10.**(HI) + 10.**(HeI) + 10.**(HeII)
    elif ion.lower() == 'hi':
        Ion_rate = 10.**(HI)
    elif ion.lower() == 'hei':
        Ion_rate = 10.**(HeI)
    elif ion.lower() == 'heii':
        Ion_rate = 10.**(HeII)
    else:
        print "What ion have you selected? I see ", ion

    ## Integrate the Ion_rate over the time of the simulation, note 't' is in years so convert to seconds
    Nion = np.trapz(Ion_rate, t * 31556925.2)

    ## What normalisation? 
    Norm = 1.
    ## Output option of Number of ionising photons per stellar mass
    if permstar != False:
        Norm = mstar
    ## Output option of Number of ionising photons per baryon
    if perbaryon != False:
        ## N_Baryon is (Msol in kg / Proton mass in kg)
        Norm = mstar * 1.1891033954016066e+57
    ## Output option of Number of ionising photons per atom
    if peratom != False:
        ## N_atom is (Msol in kg / Proton mass in kg) / 1.67
        Norm = mstar * 1.1891033954016066e+57 / 1.67

    return Nion/Norm