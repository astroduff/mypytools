import dispersion

filein = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/snapshot_045/snap_045'

res = 10.
CalcVal = 'HA'
LoSVal = 'Diff_V'
outdir = '/Volumes/Duffstore/OWLS/AGN_L200N256_WMAP7/data/render/'+"{0:.3f}".format( res )+'kpc/'

#dispersion.calcrender(filein,outdir,dlos=0.001,ndlos=20.,num_threads=4,res=res,CalcVal=CalcVal,LoSVal=LoSVal)#zoom=20.
dispersion.calcrender(filein,outdir,dlos=200.,num_threads=4,res=res,CalcVal=CalcVal,LoSVal=LoSVal)#zoom=20.

dispersion.plotrender(outdir+'Render_'+"{0:.3f}".format( res )+'kpc_'+CalcVal+'_'+LoSVal+'.npz',outdir,CalcVal=CalcVal,LoSVal=LoSVal)

