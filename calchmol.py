from mypytools import pyread_hdf5
import numpy as np
import pynbody as pn

def calchmol(filename, silent=None):

    gammaeff = 1.3333333333333
    critdens = 0.1 ## H particles per cm^3                                         
    critpres = 2300. ## cm^-3 K                                                    

    s = pn.load(filename)
    h = s.halos()

    H2Mass = np.zeros(h.nsubhalos)
    counter = 0

    eos = s.g['OnEquationOfState'] == 1
    noneos = s.g['OnEquationOfState'] != 1

    pressure = critpres * ((s.g['rho'][eos] * s.g['H'][eos] / pn.units.m_p).in_units('cm**-3') / critdens)**gammaeff

    #Pressure[oneos] = critpres * (density[oneos]/critdens)^(gammaeff)

    #pressure = np.array(s.g['p'])
#    H2HI = 1. - 1./ (1. + (np.array(s.g['p'][eos]) / 3.5e4)**(0.92) )
    H2HI = 1. - 1./ (1. + (np.array(pressure) / 3.5e4)**(0.92) )

    s.g['H2'] = s.g['Mass'].in_units('Msol')
    s.g['H2'][eos] *= ( H2HI * s.g['H'][eos] )#* s.g['Mass'][eos]
    s.g['H2'][noneos] = 0.

    for halo in h:
        try :
            for subhalo in halo.sub:
                if len(subhalo.g) > 0:
                    H2Mass[counter] = subhalo.g['H2'].sum()                
                #sub.properties['H2Mass'] = sub.g['H2'].sum()
                counter += 1
        except :
            pass
    # Now we have the masses of H2 
    return H2Mass
