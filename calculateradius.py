#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Calculate Radius from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

import numexpr as ne
import numpy as np

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'

def calculateradius(Position, Centre=None, PeriodicBoxSize=None):
    """ Calculate Radius from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

    ## Force a numpy copy to be created for each array, if present
    Position = np.array(Position).copy()
    dim_p = len(Position.shape)

    try:
        PeriodicBoxSize.size
    except:
        if not PeriodicBoxSize:
            dim_b = None
            # Didn't pass anything
        else:
            # Passed an array or scalar
            if hasattr(PeriodicBoxSize, "__len__"):
                # Array / list
                PeriodicBoxSize = np.array(PeriodicBoxSize).copy()
            else:
                # Scalar
                PeriodicBoxSize = np.array([PeriodicBoxSize])
            HalfBoxSize = PeriodicBoxSize/2.
            NegHalfBoxSize = -1. * HalfBoxSize
            dim_b = len(PeriodicBoxSize.shape)
    else:
        # Passed a numpy array
        PeriodicBoxSize = np.array(PeriodicBoxSize).copy()
        HalfBoxSize = PeriodicBoxSize/2.
        NegHalfBoxSize = -1. * HalfBoxSize        
        dim_b = len(PeriodicBoxSize.shape)

    try:
        Centre.size
    except:
        if not Centre:
            # Didn't pass anything
            dim_c = None
        else:
            # Passed an array or scalar, not a numpy array, probably list
            if hasattr(Centre, "__len__"):            
                # List or array
                Centre = np.array(Centre).copy()
            else:
                # Scalar
                Centre = np.array([Centre])
            dim_c = len(Centre.shape)
    else:
        ## Passed a numpy array
        Centre = np.array(Centre).copy()
        dim_c = len(Centre.shape)

    # Check that the Position array is ge to the Centre array
    if dim_p and dim_c:
        if dim_p >= dim_c:
            # Redefine the centre to calculate the radius from, otherwise assume [zero, zero, zero]
            Position = ne.evaluate('Position - Centre')
        else:
            print "Position is a smaller array than the Centre provided, this can't work "
            return -1

    # Periodic Boundaries
    if dim_p and dim_b:
    # Check that the Position array is ge to the Periodic array
        if dim_p >= dim_b:
            Position = ne.evaluate('where(Position > HalfBoxSize, Position - PeriodicBoxSize, Position)')
            Position = ne.evaluate('where(Position < NegHalfBoxSize, Position + PeriodicBoxSize, Position)')
        else:
            print "Position is a smaller array than the PeriodicBoxSize provided, this can't work "
            return -2

    ## X^2 + Y^2 + Z^2 (for arbitrary dimensionality, could be X^2 or X^2 + Y^2 etc)
    if dim_p > 1:
        Position = ne.evaluate('sum(Position**2,axis=1)')
    else:
        ## Special case of a single particle
        Position = ne.evaluate('sum(Position**2,axis=0)')

    return ne.evaluate('sqrt(Position)')
