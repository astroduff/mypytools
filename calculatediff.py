#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Calculate Difference from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

import numexpr as ne
import numpy as np

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'

def calculatediff(Position, Centre, PeriodicBoxSize=None):
    """ Calculate Difference from X (can be scalar or array) from Centre (if desired; scalar or array) within a PeriodicBoxSize (if desired; scalar or array)."""

    ## Force a numpy copy to be created for each array
    Position = np.array(Position).copy()
    dim_p = len(Position.shape)

    Centre = np.array(Centre).copy()
    dim_c = len(Centre.shape)

    try:
        PeriodicBoxSize.size
    except:
        if not PeriodicBoxSize:
            dim_b = None
            # Didn't pass anything
        else:
            # Passed an array or scalar
            if hasattr(PeriodicBoxSize, "__len__"):
                PeriodicBoxSize = np.array(PeriodicBoxSize).copy()
            else:
                # Scalar
                PeriodicBoxSize = np.array([PeriodicBoxSize])
            HalfBoxSize = PeriodicBoxSize/2.
            NegHalfBoxSize = -1. * HalfBoxSize
            dim_b = len(PeriodicBoxSize.shape)            
    else:
        # Passed a numpy array
        PeriodicBoxSize = np.array(PeriodicBoxSize).copy()
        HalfBoxSize = PeriodicBoxSize/2.
        NegHalfBoxSize = -1. * HalfBoxSize
        dim_b = len(PeriodicBoxSize.shape)

    # Check that the Position array is ge to the Centre array
    if dim_p >= dim_c:

        Position = ne.evaluate('Position - Centre')

        # Periodic Boundaries
        if dim_b:
    # Check that the Position array is ge to the Periodic array
            if dim_p >= dim_b:
                Position = ne.evaluate('where(Position > HalfBoxSize, Position - PeriodicBoxSize, Position)')
                Position = ne.evaluate('where(Position < NegHalfBoxSize, Position + PeriodicBoxSize, Position)')
            else:
                print "Position is a smaller array than the PeriodicBoxSize provided, this can't work "
                return -2
    else:
        print "Position is a smaller array than the Centre provided, this can't work "
        return -1

    return Position
