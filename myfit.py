
from scipy.optimize import curve_fit
from scipy.stats import lognorm
from scipy.special import airy
import lmfit as lm
import numpy as np

#from IPython import embed

def getparams(fn):
    # Get the parameter range for a given user defined function

    params = lm.Parameters()
    # Set params
    if fn == 'gaussian':
        params.add('mu', value=1.)
        params.add('sig', value=1., min=0.01)
        params.add('scale', value=1.)

    # Set params
    elif fn == 'loggaussian':
        params.add('mu', value=1.)
        params.add('sig', value=1.)

    # Set params
    elif fn == 'evd':
        params.add('mu', value=1.)
        params.add('sig', value=1.)
        params.add('scale', value=1., min=0.01)

    # Set params
    elif fn == 'landau':
        params.add('mu', value=1.)
        params.add('sig', value=1.)
        params.add('scale', value=1., min=0.01)    

    # Set params
    elif fn == 'levy':
        params.add('mu', value=1.)
        params.add('sig', value=1.)

    # Set params
    elif fn == 'logdeplt' or fn == 'deplt':
        params.add('mu', value=1.)

    elif fn == 'threepowerlaw' or fn == 'logthreepowerlaw':
        params.add('norm', value=1.)
        params.add('ind', value=1., min=-10., max=10.)
        params.add('evo', value=1., min=-10., max=10.)    

    elif fn == 'powerlaw' or fn == 'logpowerlaw':
        params.add('norm', value=1.)
        params.add('ind', value=1., min=-10., max=10.)

    else:
        print "No params available "
        print "What function did you request?"

    return params

def fitcurve(xin, datain, eps_data=None, zvalues=None, fn = 'gaussian'):
    # Use lmfit, provide the parameter constraints for each function and if no errors given just set as unity
    zval = None
    if fn == 'logpowerlaw' or fn == 'logthreepowerlaw':
        allgtzero = np.where( np.logical_and(xin > 0., datain > 0.) )
        x = np.log10(xin[allgtzero])
        data = np.log10(datain[allgtzero])
        if zvalues is not None:
            zval = np.log10(zvalues[allgtzero])
    else:
        x = xin
        data = datain
        if zvalues is not None:
            zval = zvalues

    try:
        eps_data.size
    except:
        if not eps_data:
            # Didn't pass anything, set to unity
            eps_data = np.ones(data.size)
        else:
            if hasattr(eps_data, "__len__"):            
                # Passed list, make array
                eps_data = np.array(eps_data)
            else:
                # one entry, replicate error across all
                eps_data = np.ones(data.size) * eps_data
    else:
        if hasattr(eps_data, "__len__"):            
            # Passed array
            eps_data = np.array(eps_data)
        else:
            # one entry, replicate error across all
            eps_data = np.ones(data.size) * eps_data

    params = getparams(fn)
    # minimize function 
    out = lm.minimize(residual, params, args=(x,), kws = {'data' : data, 'eps_data' : eps_data, 'fn': fn, 'zvalues' : zval})

#    print lm.fit_report(params)
    return out

def residual(params, x, data=None, eps_data=None, zvalues=None, fn='gaussian'):
    
    if zvalues is not None:
        x = np.asarray([x,zvalues])

    if fn == 'gaussian':
        model = gaussian(x, params)

    if fn == 'loggaussian':
        model = loggaussian(x, params)

    if fn == 'evd':
        model = evd(x, params)

    if fn == 'landau':
        model = landau(x, params)

    if fn == 'planckian':
        model = planckian(x, params)

    if fn == 'levy':
        model = levy(x, params)

    if fn == 'powerlaw' or fn == 'logpowerlaw':
        if fn == 'logpowerlaw':
            model = logpowerlaw(x, params)
        else:
            model = powerlaw(x, params)

    if fn == 'threepowerlaw' or fn == 'logthreepowerlaw':
        if fn == 'logthreepowerlaw':
            model = logthreepowerlaw(x, params)
        else:
            model = threepowerlaw(x, params)

    if fn == 'deplt' or fn == 'logdeplt':
        if fn == 'logdeplt':
            model = logdeplt(x, params)
        else:
            model = deplt(x, params)

    out = model
    if data is not None:
        out = model - data
    if eps_data is not None:
        out /= eps_data

    return out

# Gaussian
def gaussian(x, params):
    val = params.valuesdict()

    return val['scale'] * np.exp(-(x - val['mu'])**2. / val['sig']**2.)

# powerlaw (with two parameter)
def powerlaw(x, params):
    val = params.valuesdict()

    return val['norm'] * x**val['ind']

# log powerlaw (with two parameter)
def logpowerlaw(x, params):
    val = params.valuesdict()

    return val['norm'] + val['ind'] * x 

# powerlaw (with third parameter)
def threepowerlaw(x, params):
    val = params.valuesdict()
    
    return val['norm'] * x[0,:]**val['ind'] * x[1,:]**val['evo']
    
# log powerlaw (with third parameter)
def logthreepowerlaw(x, params):
    val = params.valuesdict()

    return val['norm'] + val['ind'] * x[0,:] + val['evo'] * x[1,:]

# Pareto
def pareto(x, params):
    val = params.valuesdict()

    return val['mu'] * val['sig']**val['mu'] / x**(val['mu']+1.)


# Cauchy
def cauchy(x, params):
    val = params.valuesdict()

    return 1./(val['sig'] * np.pi * (1. + (-val['mu'] +x)**2. / val['sig']**2.))


# Landau distribution (fat tail lognormal basically)
def landau(x, params):
    ## Approximated by Moyal formula, J.E. Moyal, Theory of ionization fluctuations, Phil. Mag. 46 (1955) 263
    ## but analytic approx too low in the tail hence modified by lambda_x fn 
    ## http://www.slac.stanford.edu/grp/ek/hippodraw/lib/classhippodraw_1_1Landau.html
    val = params.valuesdict()

    lambda_x = (x - val['mu'])/val['sig']
    moyal = np.exp(-0.5*(lambda_x + np.exp(-lambda_x)))/ np.sqrt(2.*np.pi)

    return val['scale'] * moyal


# Levy distribution (fat tail lognormal basically)
def levy(x, params):
    ## From wolfram
    val = params.valuesdict()
 
    return (np.exp(-0.5*val['sig']/(x - val['mu'])) / (x - val['mu'])**1.5) * np.sqrt(val['sig'] / (2.*np.pi))


# LogGaussian
def loggaussian(x, params):
    val = params.valuesdict()

    return np.exp(-0.5 * ( (np.log(x) - val['mu'])/val['sig'])**2.)/(x * val['sig'] * np.sqrt(2.*np.pi))


# Extreme Value Distribution
def evd(x, params):
    val = params.valuesdict()

    z = (1. + val['scale'] * (x - val['mu']) / val['sig'])**(1./val['scale'])
    return z**(val['scale'] + 1.) * np.exp(-z) / val['sig']


def planckian(x, params):
    val = params.valuesdict()
 
    return val['mu'] / (-1. + np.exp(1./x) * x**val['sig'])


# Map-Airy function
def mapairy(x, params):
    ## Has no params!
    Ai, Aip, Bi, Bip = airy(x**2.)

    return 2. * np.exp(-2. * x**3. / 3.) * (x * Ai - Aip)    


def logdeplt(x, params):
    val = params.valuesdict()

    return -1.*np.log10(1. + 1./(val['mu'] * (10.**x)) )


def deplt(x, params):
    val = params.valuesdict()

    return 1./(1. + 1./(val['mu'] * x) )
