#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Calculate Circular velocity by sorting radius, then return Vcirc to original array order """

import numexpr as ne
import numpy as np

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'

def calculatevcirc(Radius, Mass, Gunit=1.):
    """ Calculate Circular velocity by sorting radius, then return Vcirc to original array """

    Radius = np.array(Radius).copy()
    Mass = np.array(Mass).copy()
    Vcirc = np.array(Radius).copy()

    sortedarray = np.argsort(Radius)

    Radius = Radius[sortedarray]
    Vcirc = np.cumsum(Mass[sortedarray])

    Vcirc[sortedarray] = ne.evaluate('sqrt(Gunit * Vcirc / Radius)')

    ## Remove the case where radius is zero
    eqzero = Radius == 0.
    if len(Radius[eqzero]):
        Vcirc[eqzero] = 0.

    return Vcirc
